package handlers

import (
	"blob/internal/data"
	"context"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	blobQCtxKey
	horizonConfigCtxKey
)

func CtxBlobQ(entry data.BlobQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, blobQCtxKey, entry)
	}
}

func BlobQ(r *http.Request) data.BlobQ {
	return r.Context().Value(blobQCtxKey).(data.BlobQ).New()
}

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxHorizonCfg(entry map[string]interface{}) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, horizonConfigCtxKey, entry)
	}
}

func HorizonCfg(r *http.Request) map[string]interface{} {
	return r.Context().Value(horizonConfigCtxKey).(map[string]interface{})
}
