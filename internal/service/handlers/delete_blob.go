package handlers

import (
	"blob/internal/service/requests"
	"blob/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func DeleteBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewDeleteBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	resultBlob, err := BlobQ(r).DeleteById(request.BlobID)

	if resultBlob == nil {
		ape.Render(w, problems.NotFound())
		return
	}

	if err != nil {
		Log(r).WithError(err).Error("failed to get blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	response := resources.BlobResponse{
		Data: newBlobModel(*resultBlob),
	}

	ape.Render(w, response)
}
