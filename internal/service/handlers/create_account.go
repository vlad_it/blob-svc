package handlers

import (
	"blob/internal/service/requests"
	"blob/resources"
	"bytes"
	"encoding/json"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3/errors"
	depkeypair "gitlab.com/tokend/go/keypair"
	"gitlab.com/tokend/go/signcontrol"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"
	"io/ioutil"
	"net/http"
	"strconv"
)

type TransactionCore struct {
	Transaction string `json:"tx"`
}

type Details map[string]interface{}

func (d Details) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}(d))
}

func CreateAccount(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateAccountRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	txBuilder := xdrbuild.NewBuilder("TokenD Developer Network", 600000)

	tx := txBuilder.Transaction(keypair.MustParseAddress(request.Data.Attributes.SourceAccount))

	tx = tx.Op(&xdrbuild.CreateAccount{
		Destination: request.Data.Attributes.Operation.Body.CreateAccountOp.Destination,
		RoleID:      request.Data.Attributes.Operation.Body.CreateAccountOp.RoleID,
		Signers:     getSigners(request.Data.Attributes.Operation.Body.CreateAccountOp.SignersData),
	})

	envelopeMarshal, err := tx.Marshal()

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to build tx envelope"))...)
		return
	}

	var envelope xdr.TransactionEnvelope

	err = envelope.Scan(envelopeMarshal)

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.New("failed to extract envelope info"))...)
		return
	}

	signedEnvelope, err := txBuilder.Sign(&envelope, keypair.MustParseSeed("SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4"))

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to sign tx"))...)
		return
	}

	signedEnvelopeHash, err := xdr.MarshalBase64(signedEnvelope)

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to marshal tx envelope"))...)
		return
	}

	reqResult, err := json.Marshal(TransactionCore{Transaction: signedEnvelopeHash})

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to marshal core envelope"))...)
		return
	}
	horizonCfg := HorizonCfg(r)

	endpoint := horizonCfg["endpoint"].(string)
	endpoint = endpoint + ":" + strconv.Itoa(horizonCfg["addr"].(int)) + "/v3/transactions"

	response, err := PostHorizon(r, endpoint, reqResult, "SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4")
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(errors.Wrap(err, "failed to post tx"))...)
		return
	}

	w.Write(response)
}

func getSigners(accountSigners []resources.Signer) []xdrbuild.SignerData {
	var signers []xdrbuild.SignerData
	for _, accountSigner := range accountSigners {
		signers = append(signers, xdrbuild.SignerData{
			PublicKey: accountSigner.PublicKey,
			RoleID:    accountSigner.RoleID,
			Weight:    accountSigner.Weight,
			Identity:  accountSigner.Identity,
			Details:   Details{},
		})
	}
	return signers
}

func PostHorizon(r *http.Request, endpoint string, reqResult []byte, seed string) ([]byte, error) {
	request, err := http.NewRequest("POST", endpoint, bytes.NewReader(reqResult))
	if err != nil {
		return nil, errors.Wrap(err, "Failed to create POST http.Request")
	}
	request = request.WithContext(r.Context())

	request.Header.Set("content-type", "application/json")

	err = signcontrol.SignRequest(request, depkeypair.MustParse(seed))
	if err != nil {
		return nil, errors.Wrap(err, "Failed to sign request")
	}

	client := http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to perform http request")
	}

	defer request.Body.Close()

	resp, err := ioutil.ReadAll(response.Body)

	return resp, nil
}
