package handlers

import (
	"blob/internal/service/requests"
	"blob/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func GetBlobList(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetBlobListRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blob := BlobQ(r)

	blob.Page(request.OffsetPageParams)
	if len(request.FilterOwner) > 0 {
		blob.FilterByOwner(request.FilterOwner...)
	}

	blobs, err := blob.Select()

	if err != nil {
		Log(r).WithError(err).Error("failed to get blobs")
		ape.Render(w, problems.InternalError())
		return
	}

	newList := make([]resources.Blob, len(blobs))
	for i, b := range blobs {
		newList[i] = newBlobModel(b)
	}

	response := resources.BlobListResponse{
		Data:  newList,
		Links: GetOffsetLinks(r, request.OffsetPageParams),
	}

	ape.Render(w, response)
}
