package handlers

import (
	"blob/internal/data"
	"blob/internal/service/requests"
	"blob/resources"
	"encoding/json"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateBlobRequest(r)
	if err != nil {
		Log(r).WithError(err).Info("wrong request")
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	var resultBlob data.Blob

	blob := data.Blob{
		Owner: request.Relationships.Owner,
		Data:  json.RawMessage(request.Attributes),
	}
	resultBlob, err = BlobQ(r).Insert(blob)

	if err != nil {
		Log(r).WithError(err).Error("failed to insert blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	response := resources.BlobResponse{
		Data: newBlobModel(resultBlob),
	}
	ape.Render(w, response)

}
func newBlobModel(blob data.Blob) resources.Blob {
	result := resources.Blob{
		Key:           resources.NewKeyInt64(blob.ID, resources.BLOB),
		Relationships: resources.BlobRelationships{Owner: blob.Owner},
		Attributes:    json.RawMessage(blob.Data),
	}

	return result
}
