package requests

import (
	"blob/resources"
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/http"
)

type CreateAccountReq struct {
	Data resources.CreateAccount
}

func NewCreateAccountRequest(r *http.Request) (CreateAccountReq, error) {
	var request CreateAccountReq

	err := json.NewDecoder(r.Body).Decode(&request.Data)

	if err != nil {
		return request, errors.Wrap(err, "failed to unmarshal")
	}

	return request, nil
}
