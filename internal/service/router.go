package service

import (
	"blob/internal/data/postgres"
	"blob/internal/service/handlers"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobQ(postgres.NewBlobsQ(s.cfg.DB())),
			handlers.CtxHorizonCfg(s.cfg.Horizon()),
		),
	)
	r.Route("/blob", func(r chi.Router) {
		// configure endpoints here
		r.Post("/", handlers.CreateBlob)
		r.Get("/{id}", handlers.GetBlob)
		r.Delete("/delete/{id}", handlers.DeleteBlob)
	})

	r.Get("/test", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome"))
	})

	r.Get("/blobList", handlers.GetBlobList)
	r.Post("/accounts", handlers.CreateAccount)
	return r
}
