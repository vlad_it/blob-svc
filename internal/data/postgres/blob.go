package postgres

import (
	"blob/internal/data"
	"database/sql"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
)

const blobsTableName = "blobs"

type blobsQ struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func NewBlobsQ(db *pgdb.DB) data.BlobQ {
	return &blobsQ{
		db:  db.Clone(),
		sql: sq.Select("n.*").From(fmt.Sprintf("%s as n", blobsTableName)),
	}
}
func (q *blobsQ) New() data.BlobQ {
	return NewBlobsQ(q.db)
}

func (q *blobsQ) Get() (*data.Blob, error) {
	var result data.Blob
	err := q.db.Get(&result, q.sql)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &result, err
}

func (q *blobsQ) Select() ([]data.Blob, error) {
	var result []data.Blob
	err := q.db.Select(&result, q.sql)
	return result, err
}

func (q *blobsQ) Transaction(fn func(q data.BlobQ) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}

func (q *blobsQ) Insert(value data.Blob) (data.Blob, error) {
	var result data.Blob

	stmt := sq.Insert(blobsTableName).SetMap(map[string]interface{}{
		"data":  value.Data,
		"owner": value.Owner,
	}).Suffix("returning *")
	err := q.db.Get(&result, stmt)

	return result, err
}

func (q *blobsQ) FilterByID(ids ...int64) data.BlobQ {
	q.sql = q.sql.Where(sq.Eq{"n.id": ids})
	return q
}

func (q *blobsQ) FilterByOwner(owners ...string) data.BlobQ {
	q.sql = q.sql.Where(sq.Eq{"n.owner": owners})
	return q
}

func (q *blobsQ) DeleteById(ids ...int64) (*data.Blob, error) {
	blob, err := q.FilterByID(ids...).Get()
	if err != nil {
		return nil, err
	}
	//err = q.db.ExecRaw(`DELETE FROM `+blobsTableName+` WHERE id = $1;`, ids[0])
	stmt := sq.Delete(blobsTableName).Where(sq.Eq{"id": ids[0]})
	err = q.db.Exec(stmt)
	return blob, err
}
func (q *blobsQ) Page(pageParams pgdb.OffsetPageParams) data.BlobQ {
	q.sql = pageParams.ApplyTo(q.sql, "id")
	return q
}
