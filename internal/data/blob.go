package data

import (
	"encoding/json"
	"gitlab.com/distributed_lab/kit/pgdb"
)

type BlobQ interface {
	New() BlobQ

	Get() (*Blob, error)
	Select() ([]Blob, error)

	Transaction(fn func(q BlobQ) error) error

	Insert(data Blob) (Blob, error)

	FilterByID(id ...int64) BlobQ
	DeleteById(id ...int64) (*Blob, error)
	FilterByOwner(owners ...string) BlobQ

	Page(pageParams pgdb.OffsetPageParams) BlobQ
}

type Blob struct {
	ID    int64           `db:"id" structs:"-"`
	Owner string          `db:"owner" structs:"-"`
	Data  json.RawMessage `db:"data" structs:"-"`
}
