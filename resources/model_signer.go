/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Signer struct {
	Details   string `json:"details"`
	Identity  uint32 `json:"identity"`
	PublicKey string `json:"publicKey"`
	RoleID    uint64 `json:"roleID"`
	Weight    uint32 `json:"weight"`
}
