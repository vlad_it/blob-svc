/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type AccountOp struct {
	Destination string   `json:"destination"`
	Referrer    string   `json:"referrer"`
	RoleID      uint64   `json:"roleID"`
	SignersData []Signer `json:"signersData"`
}
